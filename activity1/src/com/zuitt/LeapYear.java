package com.zuitt;

import java.util.Scanner;

public class LeapYear {
    public static void main(String[] args){
        Scanner year = new Scanner(System.in);

        System.out.println("Input year to be checked if a leap year.");
        int verifyYear = year.nextInt();

        if(verifyYear % 4 == 0 && verifyYear % 100 != 0 || verifyYear % 400 ==0) {
            System.out.println(verifyYear + " is a leap year");
        }else {
            System.out.println(verifyYear + " is NOT a leap year");
        }

    }
}
