package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class PrimeNumber {
    public static void main(String[] args){
        int[] primeArrays = new int[5];

        primeArrays[0] = 2;
        primeArrays[1] = 3;
        primeArrays[2] = 5;
        primeArrays[3] = 7;
        primeArrays[4] = 9;

        System.out.println("The first prime number is " + primeArrays[0]);

        ArrayList<String> sampleNames = new ArrayList<String>(Arrays.asList("John","Jane", "Chloe","Zoey"));
        System.out.println("My friends are: "+sampleNames);

        HashMap<String,Integer> genericsHashMap = new HashMap<String,Integer>(){{
            put("toothpaste",15);
            put("toothbrush",20);
            put("soap",12);
        }};

        System.out.println("Our current inventory consists of: "+genericsHashMap);
    }
}
